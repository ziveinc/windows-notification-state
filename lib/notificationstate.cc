#include <node.h>
#include <v8.h>
#include <nan.h>

#ifdef _WIN32
#include "notificationstate-query.h"
#include "focus_assist_state.h"
#endif

using namespace v8;

NAN_METHOD(GetFocusAssistState) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);
  int returnValue = -1;
  returnValue = get_focus_assist_state();
  info.GetReturnValue().Set(Int32::New(isolate, returnValue));
}

NAN_METHOD(GetNotificationState) {
  Isolate* isolate = Isolate::GetCurrent();
  HandleScope scope(isolate);
  int returnValue = -1;
  returnValue = queryUserNotificationState();
  info.GetReturnValue().Set(Int32::New(isolate, returnValue));
}

NAN_MODULE_INIT(Init) {
  Nan::SetMethod(target, "getNotificationState", GetNotificationState);
  Nan::SetMethod(target, "getFocusAssistState", GetFocusAssistState);
}

NODE_MODULE(quiethours, Init)
